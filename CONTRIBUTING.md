# Pour ajouter des métadonnées

1. Créer une nouvelle branche (nommée par exemple selon le code réseau)
2. Ajouter le fichier et faire le commit
3. Faire un Merge Request à partir de l'interface web du projet
4. Si l'intégration continue est OK, réaliser le merge
5. Attribuer la mergre request à un opérateur nœud B (@joschaeffer ou @wolynied)
