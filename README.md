# Résif / Datacite

Ce dépôt contient : 

* dans le répertoire **sources** l'ensemble des fichiers de description pour les DOI de RESIF au format datacite.xml 4.3 (http://schema.datacite.org/meta/kernel-4/metadata.xsd")
* dans le répertoire **util** des éléments divers destiné à aider la création et la gestion des DOI de RESIF, qui a ce jour reste manuelle.

## Notes
* Le **préfixe** pour les DOi de RESIF est **10.15778**
* Le schéma du **suffixe** pour les DOI RESIF est : RESIF.NNYYYY ou RESIF.NNYYYY (exemple : RESIF.XF2018 ou RESIF.FR)
* Les fichiers de métadonnées pour les DOI RESIF dans sources ont pour extension:  **.xml**
* Les landing pages des DOI RESIF sont : **la page du réseau sur le portail seismology.resif.fr (qui sont automatiquement mises à jour par moissonnage du webservice station. Il n'y a donc JAMAIS de création de landing pages pour les DOI RESIF, ni de mise à jour**.
* Les métadonnées au format XML DataCite suivent les recommandations de la FDSN : http://doi.org/10.7914/D11596
* Voir aussi la page : https://www.fdsn.org/services/doi/
* Prendre l'habitude de toujours tester un DOI sur la plateforme de test de DataCite (https://doi.test.datacite.org) avant d'utiliser la plateforme de production.


## Procédure de demande de DOI

Il faut avant tout disposer du code réseau (appelons le XX pour la suite), et l'année de début tel que déclaré à la FDSN (YYYY).

Pour demander la création d'un DOI, il faut préparer un fichier de métadonnées au format DataCite, qui se conforme également au référentiel de nommage gatito (en particulier pour les organisations)


1. Créer une branche de ce dépôt partant de la branche par défaut (master)
2. En utilisant le template, créer un nouveau fichier de métadonnées à enregistrer sous `sources/RESIF.XXYYYY.xml`
    cp template/seismological_network.xml  sources/RESIF.XXYYYY.xml
3. Éditer le contenu du fichier. Lorsque cela vous parait complet, créer une merge request à assigner à Jonathan (@joschaeffer)

## Création d'un fichier DOI


### Initialisation

Pour initialiser le fichier datacite d'un réseau temporaire : utiliser l'un des fichiers sources/RESIF.NNYYYY.xml (NNYYYY = code FDSN étendu) récent en le renommant.

### Extraction des informations

Pour récupérer les informations dans resifInv-Prod (on prend l'exemple du réseau XF)

```
psql -U resifinv_ro -d resifInv-Prod -h postgres-geodata.ujf-grenoble.fr -c "select * from networks where network='XF';"

network_id | network | start_year | end_year |                   description                    |      starttime      |          endtime           | policy | altcode | histcode | nb_station 
-----------+---------+------------+----------+--------------------------------------------------+---------------------+----------------------------+--------+---------+----------+------------
        61 | XF      |       2018 |     2019 | Hazard in Tanzanian Rift : HATARI (RESIF-SISMOB) | 2018-01-01 00:00:00 | 2019-12-31 23:59:59.999999 | closed | HATARI  |          |          8
(1 row)
```

#### Date de démarrage

Pour récupérer la date de démarrage (on prend la plus petite date pour une station)   

```
 psql -U resifinv_ro -d resifInv-Prod -h postgres-geodata.ujf-grenoble.fr -c "select min(starttime) from station where network_id='61';"
 
         min         
---------------------
 2018-02-07 13:00:00
```

#### Nombre de stations

Pour récupérer le nombre de stations :

```
psql -U resifinv_ro -d resifInv-Prod -h postgres-geodata.ujf-grenoble.fr -c "select count(*) from station where network_id='61';"

 count 
-------
     8
(1 row)
```

#### Bounding box

Pour récupérer la bounding box d'un réseau :

1. Récupérer le network_id :
```
select * from networks where network='XK';
```

2. Récupérer les coordonnées de la box :
```
select  min(longitude), max(longitude) , min(latitude), max(latitude)  from ws_common where network_id= ....;
```

#### Size

Pour récupérer les informations de volumétrie (size) :
```
ssh sysop@resif-vm33.u-ga.fr 'du -ksh /mnt/auto/archive/data/bynet/XF2018'

101G	/mnt/auto/archive/data/bynet/XF2018
```

### Validation du fichier

Pour corriger/valider la structure XML du fichier : 

`xmllint --schema http://schema.datacite.org/meta/kernel-4/metadata.xsd  RESIF.ZK2017.xml`

## Enregistrement du DOI sur DataCite
### Méthode "automatique" (recommandée)

#### Préparation

1. Installer l'outil **resif-datacite-cli** depuis https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif-datacite-cli

2. Configurer les informations d'accès au compte DataCite selon l'une des méthodes suivantes :
- en variables d'environnement
- dans un fichier **.env** 
- en paramètre de ligne de commande de l'outil.

#### Validation d'un fichier XML

Utiliser la commande **validate** de l'outil sur le fichier à valider :
```
resif-datacite-cli validate ../datacite/sources/RESIF.ZK2017.xml
```

Note: il est possible de traiter un ensemble de fichiers en indiquant en le dossier visé plutôt qu'un fichier précis.

L'outil contrôlera si :
- la forme de l'identifiant DOI est correcte
- la structure du XML est valide (il n'est donc plus nécessaire de lancer xmllint)
- le DOI est déjà enregistré sur DataCite, et si oui, si l'URL de landing page est correcte.

#### Enregistrement/Mise à jour d'un DOI sur DataCite

Utiliser la commande **upload** de l'outil sur le fichier à traiter :
```
resif-datacite-cli upload ../datacite/sources/RESIF.ZK2017.xml
```

Note: il est possible de traiter un ensemble de fichiers en indiquant en le dossier visé plutôt qu'un fichier précis.

L'outil appliquera la procédure suivante :
- vérification du DOI et du fichier (voir ci-dessus)
- envoi du fichier XML
- assignation du DOI et de l'URL de landing page (automatiquement générée).

Le DOI résultant sera disponible à l'état **findable**.

### Méthode "manuelle"

*/!\ Cette méthode est déconseillée car le remplissage manuel ou par copier/coller augmente le risque de dépôt d'un DOI érroné.*

#### Création du DOI

Ouvrir un navigateur Web et se connecter à la page suivante:
```
https://doi.datacite.org/repositories/inist.resif/dois
DOI
Create (File Upload)
```
Puis suivre les instructions suivantes :
- Renseigner : DOI : ne pas se tromper !!!
- Choisir Draft
- URL : https://seismology.resif.fr/networks/#/XT__2018
- Upload File
- Create.

Si tout c'est bien passé :
* Passer le DOI de **Draft** a **Registered** en prenant le menu : Update Doi (File upload)
* Idem pour le passer en Findable (attention, il ne sera plus modifiable !!!)

## Enregistrement du DOI sur le portail seismology.resif.fr

- Mettre à jour la table des networks
```
cd resifInv
git pull
resifInv/admin/networks.lst
```

- Mettre à jour : le portail seismology.resif.fr pour la liste des DOI 
page statique :https://data.datacite.org/application/vnd.datacite.datacite+xml/10.15778/RESIF.ZI2001
Créer/éditer la ligne pour ce DOI, en faisant attention aux liens
