<?xml version="1.0" encoding="UTF-8"?>
<resource xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://datacite.org/schema/kernel-4" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.15778/RESIF.ZI2001</identifier>
  <creators>
    <creator>
      <creatorName nameType="Personal">Barruol, G.</creatorName>
      <givenName>G.</givenName>
      <familyName>Barruol</familyName>
    </creator>
    <creator>
      <creatorName>PLUME Group</creatorName>
    </creator>
  </creators>
  <titles>
    <title>PLUME - Polynesia Lithosphere and Upper Mantle Experiment, 2001-2005, code ZI, funded by ACI Jeunes Chercheurs, Université de Montpellier II, EOPG Strasbourg</title>
  </titles>
  <publisher>RESIF - Réseau Sismologique et géodésique Français</publisher>
  <publicationYear>2007</publicationYear>
  <resourceType resourceTypeGeneral="Other">Terrestrial seismic network</resourceType>
  <subjects>
    <subject>Mantle plume</subject>
    <subject>South Pacific Superplume</subject>
    <subject>Lithosphere and Asthenosphere</subject>
    <subject>Upper and lower mantle tomography</subject>
    <subject>Seismic anisotropy and mantle flow</subject>
    <subject>Body and surface wave seismology</subject>
    <subject>French Polynesia</subject>
    <subject>Society Hotspot</subject>
    <subject>Swell induced microseismic noise</subject>
  </subjects>
  <language>en</language>
  <contributors>
    <contributor contributorType="Other">
      <contributorName>RESIF Information System</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>RESIF Data Centre</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Université De Grenoble Alpes</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Parc D'Instruments Sismologiques Mobiles INSU (RESIF-SISMOB)</contributorName>
    </contributor>
    <contributor contributorType="DataCollector">
      <contributorName nameType="Personal">Réseau Large-Bande Mobile, Institut De Physique Du Globe De Strasbourg</contributorName>
      <givenName>Institut De Physique Du Globe De Strasbourg</givenName>
      <familyName>Réseau Large-Bande Mobile</familyName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Université De Strasbourg</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Université De Polynésie Française</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Université De Montpellier II</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Parc D'Instruments Sismologiques Mobiles (RESIF-SISMOB)</contributorName>
    </contributor>
    <contributor contributorType="Sponsor">
      <contributorName nameType="Personal">Ministère De La Recherche, Action Concertée Incitative (ACI) Jeunes Chercheurs</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>Ministère De L'Outre Mer</contributorName>
    </contributor>
    <contributor contributorType="Other">
      <contributorName>CNRS INSU (Institut National Des Sciences De L'Univers)</contributorName>
    </contributor>
  </contributors>
  <dates>
    <date dateType="Collected">2001-04-05/2005-12-31</date>
    <date dateType="Available">2007-12-31</date>
    <date dateType="Issued">2007</date>
  </dates>
  <relatedIdentifiers>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2002eo000354</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1111/j.1365-246x.2006.02871.x</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2005eo440001</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1111/j.1365-246x.2006.03037.x</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1016/j.epsl.2006.07.010</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:1210.1111/j.1365-1246x.2007.03475.x</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1016/j.pepi.2008.10.016</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2008jb005709</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2009gl037568</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2009gl038139</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2009gc002533</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1029/2010gl042534</relatedIdentifier>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsReferencedBy">doi:10.1146/annurev-earth-060313-054818</relatedIdentifier>
  </relatedIdentifiers>
  <version/>
  <rightsList>
    <rights rightsURI="info:eu-repo/semantics/openAccess">Open Access</rights>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0">Creative Commons By 4.0 Universal</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">PLUME (Polynesian Lithosphere and Upper Mantle Experiment), has the objective of imaging the upper mantle structures beneath French Polynesia. This region of the south Pacific, which is far from any plate boundary, comprises oceanic lithosphere with ages varying between 30 and 100 Ma, as well as two major fracture zones. The area has been affected by a "swarm" of volcanic islands chains - the Society Islands, Austral Islands, and Marquesas- that may represent hotspot tracks [Duncan and McDougall, 1976]. The individual hotspots are superimposed on the large South Pacific Superswell [McNutt, 1998]. The region is also characterized by a large-scale, low-velocity anomaly in the lowermost mantle. These observations have been interpreted as evidence of a lower-mantle “super-plume” that is at least partially blocked in the transition zone and crowned by several small-scale "upper mantle" plumes that give rise to the hot spot tracks observed on the surface. 
The aim of the PLUME experiment is to characterize the interaction between mantle plumes and lithosphere, to probe the interaction between mantle plumes and the large-scale mantle flow, to image the geometry of plumes in the upper mantle and their eventual connection with the South Pacific super-plume, and to quantify the mass transfers through the transition zone.
The PLUME seismic network was composed of 10 broadband stations equipped with STS-2 seismic sensors deployed in the various archipelagos of French Polynesia for the period October 2001 to August 2005. The region under study covers an area equivalent in size to Europe, with a spacing of a few hundreds of kilometers between the temporary stations. The deployment of the PLUME network has been designed to supplement the permanent IRIS (RAR, PTCN, XMAS), Geoscope (PPT, TAOE), and Commissariat à l'Energie Atomique (TBI, RKT, PPTL, TPT) stations available in the region, providing more homogeneous instrument coverage of the entire area. The terrestrial part of the PLUME experiment has been complemented by several ocean bottom deployments of seismic stations by the JAMSTEC group: 8 OBS were deployed over the whole French Polynesia area in 2003-2004, 2 in the vicinity of Tahiti in 2004-2005 and 9 in the vicinity of the Society hotspot in the frame of the TIARES network in 2009-2010 [Suetsugu et al., 2012].</description>
  </descriptions>
  <geoLocations>
    <geoLocation>
      <geoLocationPlace>French Polynesia, Tahiti, Society, Australs, Gambier, Tuamotus, Marquesas</geoLocationPlace>
    </geoLocation>
  </geoLocations>
</resource>
