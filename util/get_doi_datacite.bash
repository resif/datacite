#!/bin/bash
# récupération des doi resif sur le site de datacite
cat ~/resifInv/admin/networks.lst | sed "/^\#/d" | cut -d'|' -f 10 | while read fdoi; do
doi=`basename $fdoi`
if [[ $fdoi != "#" ]] ; then
doi=`basename $fdoi`
wget "https://data.datacite.org/application/vnd.datacite.datacite+xml/$fdoi" -O $doi.xml
fi
done

